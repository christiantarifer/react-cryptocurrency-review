import React, { useEffect, useState } from 'react';


// ********************************** THIRD PARTY ********************************* //

import styled from '@emotion/styled';
import axios from 'axios';

// **************************** CUSTOM HOOKS ************************************** //

import useMoneda from '../hooks/useMoneda';
import useCriptomoneda from '../hooks/useCriptomoneda';

// **************************** CUSTOM COMPONENTS ************************************** //

import Error from './Error';

// ******************************************************************************** //

const Boton = styled.input`
    margin-top: 20px;
    font-weight: bold;
    font-size: 20px;
    padding: 10px;
    background-color: #66a2fe;
    border: none;
    width: 100%;
    border-radius: 10px;
    color; #FFF;
    transition: background-color .3s ease;

    &:hover{
        background-color: #326AC0;
        cursor: pointer;
    }
`;

const Formulario = ( { guardarMoneda, guardarCriptomoneda } ) => {

    // state Cryptocurrency list
    const [ listadocripto, guardarCriptomonedas ] = useState([]);

    const [ error, guardarError ] = useState( false );

    const MONEDAS = [
        { codigo: 'USD', nombre: 'Dolar de Estados Unidos' },
        { codigo: 'MXN', nombre: 'Peso Mexicano' },
        { codigo: 'EUR', nombre: 'Euro' },
        { codigo: 'GBP', nombre: 'Libra Esterlina'  }
    ]

    // USE useMoneda
    const [ moneda, SelectMonedas ] = useMoneda('Elige tu moneda', '', MONEDAS);

    // USE useCriptomoneda
    const [ criptomoneda, SelectCripto ] = useCriptomoneda('Elige tu Criptomoneda', '', listadocripto );

    // EXECUTE API CALL
    useEffect( () => {
        const consultarAPI = async () => {
            const url = `https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD`;

            const resultado = await axios.get(url);


            guardarCriptomonedas(resultado.data.Data)
        }

        consultarAPI();

    }, [] );

    // WHEN USERS SUBMITS FORM

    const cotizarMoneda = e => {

        e.preventDefault();

        // VALIDATE IF BOTH FIELDS ARE FILLED
        if( moneda === '' || criptomoneda === '' ) {

            guardarError( true );
            return;

        }

        // PASS DATA TO MAIN COMPONENT
        guardarError( false );

        guardarMoneda( moneda );
        guardarCriptomoneda( criptomoneda );

    }

    return ( 
        <form
            onSubmit={ cotizarMoneda }
        >

            { error ? <Error mensaje="Todos los campos son obligatorios"/> : null }

            <SelectMonedas />

            <SelectCripto />
            <Boton 
                type="submit"
                value="Calcular"
            />

        </form>
     );
}
 
export default Formulario;

