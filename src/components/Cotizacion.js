import React from 'react';

// ********************************** THIRD PARTY ********************************* //

import styled from '@emotion/styled';

// ******************************************************************************** //

const ResultadoDiv = styled.div`
    color: #FFF;
    font-family: Arial, Helvetica, sans-serif;

`;

const Info = styled.p`
    font-size: 18px;

    span{
        font-weight: bold;
    }
`

const Precio = styled.p`
    font-size: 30px;
    span{
        font-weight: bold;
    }
`

const Cotizacion = ({ resultado }) => {

    if( Object.keys(resultado).length === 0 ) return null;

    console.group('--------------------------------------')
    console.log( resultado );
    console.groupEnd();

    return (
        <ResultadoDiv>
            <Precio>El precio es: <span>{ resultado.PRICE }</span></Precio>
            <Info>Precio m&aacute;s alto del d&iacute;a: <span>{ resultado.HIGHDAY }</span></Info>
            <Info>Precio m&aacute;s bajo del d&iacute;a: <span>{ resultado.LOWDAY }</span></Info>
            <Info>Variaci&oacute;n ultimas 24 horas: <span>{ resultado.CHANGEPCT24HOUR }</span></Info>
            <Info>&Uacute;ltima actualizaci&oacute;n: <span>{ resultado.LASTUPDATE }</span></Info>
        </ResultadoDiv>
    );
    
}
 
export default Cotizacion;