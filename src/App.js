import React, { useEffect, useState } from 'react';

// ******************************* THIRD PARTY PACKAGES ************************************* //
import styled from '@emotion/styled';
import axios from 'axios';

// ****************************************** MULTIMEDIA ********************************** //

import imagen from './cryptomonedas.png';

// ******************************** COMPONENTS ********************************* //

import Formulario from './components/Formulario';
import Cotizacion from './components/Cotizacion';
import Spinner from './components/Spinner';

// *************************************************************************************** //


const Contenedor = styled.div`
  max-width: 900px;
  margin: 0 auto;
  @media (min-width: 992px){
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    column-gap: 2rem;
  };
`;

const Imagen = styled.img`
  max-width: 100%;
  margin-top: 5rem;

`

const Heading = styled.h1`
  font-family: 'Bebas Neue', cursive;
  color: #FFF;
  text-align: left;
  font-weight: 700;
  font-size: 50px;
  margin-bottom: 50px;
  margin-top: 80px;

  &::after{
    content: '';
    width: 100px;
    height: 6px;
    background-color: #66A2FE;
    display: block;
  }
`;

function App() {

  const [ moneda, guardarMoneda ] = useState('');

  const [ criptomoneda, guardarCriptomoneda ] = useState('');

  const [ resultado, guardarResultado ] = useState({});

  const [cargando, guardarCargando] = useState(false)

  useEffect(() => {

    const cotizarCriptomoneda = async () => {

      // AVOID FIRST EXECUTION
      if( moneda === '' ) return;

      console.group('Main component');
      console.log('Cotizando...');

      // ACCESS API
      const url = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${criptomoneda}&tsyms=${moneda}`;

      const resultado = await axios.get(url);

      // SHOW SPINNER
      guardarCargando(true);

      // HIDE SPINNER AND SHOW RESULT
      setTimeout(() => {

        // CHANGE cargando STATE
        guardarCargando(false);
        
        // GUARDAR COTIZACION
        guardarResultado(resultado.data.DISPLAY[criptomoneda][moneda]);

      }, 3000 );
      
      console.groupEnd();

    }

    cotizarCriptomoneda();
    
  }, [moneda, criptomoneda]);

  // SHOW SPINNER OR RESULT 
  const componente = ( cargando ) ? <Spinner/> : <Cotizacion resultado={ resultado } />
  

  return (
    <Contenedor>
      <div>
        <Imagen src={imagen} alt="imagen crypto"/>
      </div>
      <div>
        <Heading>Cotiza Criptomonedas Al instante</Heading>

        <Formulario
          guardarMoneda={ guardarMoneda }
          guardarCriptomoneda = { guardarCriptomoneda }
        />

        
        {componente}

      </div>
    </Contenedor>
  );
}

export default App;
